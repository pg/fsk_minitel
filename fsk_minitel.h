
typedef unsigned char uchar, uint8;
typedef signed char schar, int8;
typedef unsigned short ushort, uint16;
typedef short int16;
typedef unsigned int uint;
typedef unsigned long ulong;
#if UINT_MAX >= 0xFFFFFFFF
typedef int int32;
typedef unsigned int uint32;
#else
typedef long int32;
typedef unsigned long uint32;
#endif
typedef long long int64;
typedef unsigned long long uint64;


typedef struct tTx
{
  enum
  {
    stSendingOnes,
    stSendingData
  } State;

  uint SampleRate;
  uint OnesFreq;
  uint ZeroesFreq;
  uint BitRate;

  uint32 SampleCnt;
  uint BitSampleCnt;
  uint Data;
  uint DataLeft;

  double Phase;
  double PhaseIncrement;

  queue_state_t *queue;
  uint (*pTxGetDataCallBack)(struct tTx*, uint8*);
} tTx;

typedef struct tRx
{
  enum
  {
    stCarrierLost,
    stCarrierDetected,
    stReceivingData
  } State;

  uint SampleRate;
  uint OnesFreq;
  uint ZeroesFreq;
  uint MidFreq;
  uint BitRate;

  uint32 SampleCnt;
  uint BitSampleCnt;
  uint Data;

  double Phase;
  double PhaseIncrement;

  tComplex Filter[FILTER_LENGTH];
  double Delay[FILTER_LENGTH];

  double LastAngle;
  int LastDelta;
  int32 Deltas;

  int32 CarrierAngle;
  int32 CarrierCnt;

  double LongAvgPower;
  double ShortAvgPower;
  queue_state_t *queue;
  void (*pRxGetDataCallBack)(struct tRx*, uint8);
} tRx;


void TxInit(tTx* pTx,
            uint SampleRate,
            uint (*pTxGetDataCallBack)(tTx*, uint8*));

int16 TxGetSample(tTx* pTx);

void RxInit(tRx* pRx,
            uint SampleRate,
            void (*pRxGetDataCallBack)(struct tRx*, uint8));

void RxGetSample(tRx* pRx, int16 Sample);

